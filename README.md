For over 30 years, Adirondack Audiology Associates has been helping improve the quality of life for our patients in Colchester, Plattsburgh, Saranac Lake, Potsdam and the surrounding areas of Vermont and New York.

Address: 144 Broadway, Suite 1, Saranac Lake, NY 12983, USA

Phone: 518-891-0487
